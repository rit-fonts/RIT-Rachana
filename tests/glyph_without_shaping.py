# Report glyphs (with no Unicode code point) without shaping rules associated in a font
#
# Copyright 2021 Rajeesh KV <rajeeshknambiar@gmail.com>
# This file is licensed under GPLv3.

# Usage: $0 font1 [font2 ...]

import sys, os
import fontforge as ff

#These glyphs do not have Unicode or shaping rules
IGNORE_GLYPHS = [".notdef", ".null", "NULL", "r4", "l4"]

#Malayalam Unicode code points and corresponding glyphnames
ML_GLPYH_UC_MAP = {
                   3328: 'anusvaraabove', 3329: 'mlchandrabindu', 3330: 'anusvara', 3331: 'visarga', 3333: 'ml_a', 3334: 'ml_aa',
                   3335: 'ml_i', 3336: 'ml_ii', 3337: 'ml_u', 3338: 'ml_uu', 3339: 'ml_r', 3340: 'ml_l', 3342: 'ml_e', 3343: 'ml_ee',
                   3344: 'ml_ai', 3346: 'ml_o', 3347: 'ml_oo', 3348: 'ml_au', 3349: 'k1', 3350: 'k2', 3351: 'k3', 3352: 'k4',
                   3353: 'ng', 3354: 'ch1', 3355: 'ch2', 3356: 'ch3', 3357: 'ch4', 3358: 'nj', 3359: 't1', 3360: 't2', 3361: 't3',
                   3362: 't4', 3363: 'nh', 3364: 'th1', 3365: 'th2', 3366: 'th3', 3367: 'th4', 3368: 'n1', 3369: 'n2', 3370: 'p1',
                   3371: 'p2', 3372: 'p3', 3373: 'p4', 3374: 'm1', 3375: 'y1', 3376: 'r3', 3377: 'rh', 3378: 'l3', 3379: 'lh',
                   3380: 'zh', 3381: 'v1', 3382: 'z1', 3383: 'sh', 3384: 's1', 3385: 'h1', 3386: 'rh_half', 3387: 'verticalbarvirama',
                   3388: 'circularvirama', 3389: 'avagraha', 3390: 'a2', 3391: 'i1', 3392: 'i2', 3393: 'u1', 3394: 'u2', 3395: 'r1',
                   3396: 'r2', 3398: 'e1', 3399: 'e2', 3400: 'ai1', 3402: 'o1', 3403: 'o2', 3404: 'au1', 3405: 'xx', 3406: 'dotreph',
                   3407: 'para', 3412: 'm1cil', 3413: 'y1cil', 3414: 'zhcil', 3415: 'au2', 3416: 'arakaani', 3417: 'aramaa',
                   3418: 'muunnukaani', 3419: 'orumaa', 3420: 'rantumaa', 3421: 'muunnumaa', 3422: 'naalumaa', 3423: 'archaic_ii',
                   3424: 'ml_rr', 3425: 'ml_ll', 3426: 'l1', 3427: 'l2', 3430: 'ml_0', 3431: 'ml_1', 3432: 'ml_2', 3433: 'ml_3',
                   3434: 'ml_4', 3435: 'ml_5', 3436: 'ml_6', 3437: 'ml_7', 3438: 'ml_8', 3439: 'ml_9', 3440: 'ml_10', 3441: 'ml_100',
                   3442: 'ml_1000', 3443: 'ml_quarter', 3444: 'ml_half', 3445: 'ml_3quarter', 3446: 'maakaani', 3447: 'arakkaal',
                   3448: 'muntaani', 3449: 'datemark', 3450: 'nhcil', 3451: 'n1cil', 3452: 'r3cil', 3453: 'l3cil', 3454: 'lhcil',
                   3455: 'k1cil', 8204: 'ZWNJ', 8205: 'ZWJ'
                  }

COLR_OK = '\033[92m'    #Green
COLR_WARN = '\033[93m'  #Amber
COLR_RESET = '\033[0m'

def check_ml_glyphs_sans_shaping(font_file):
    if not os.path.exists(font_file):
        print(f"Font file {font_file} not found")
        return False
    font = ff.open(font_file)
    print("Checking glyphs without codepint and substitution rules in " + font.fontname)
    error_count = 0
    for glyph in font.glyphs():
        if glyph.unicode == -1 and glyph.glyphname not in IGNORE_GLYPHS:     #TODO: check glyph.altuni too?
            subrules = glyph.getPosSub("*")     #Returns (("lookupname", "LookupType", ...),), where LookupType is one of "Position", "Pair", "Substitution", "AltSubs", "MultSubs", "Ligature".
            lookuptypes = list(map(lambda i:i[1], subrules))
            if not any(l in ["Substitution", "MultSubs", "Ligature"] for l in lookuptypes):
                error_count += 1
                print ("\t" + COLR_WARN + str(glyph.originalgid) + ": " + glyph.glyphname + COLR_RESET + " has no Unicode codepoint or substitution rule")
    return error_count

def check_ml_glyph_names_unicode(font_file):
    if not os.path.exists(font_file):
        print(f"Font file {font_file} not found")
        return False
    font = ff.open(font_file)
    print("Checking Malayalam glyph names and corresponding Unicode code points in " + font.fontname)
    error_count = 0
    for glyph in filter(lambda g: g.unicode != -1, font.glyphs()):
        std_glyphname = ML_GLPYH_UC_MAP.get(glyph.unicode, False)
        if std_glyphname and std_glyphname != glyph.glyphname:
            error_count += 1
            print ("\t" + COLR_WARN + str(glyph.unicode) + ": " + glyph.glyphname + COLR_RESET + " does not match with standardized name " + COLR_OK + std_glyphname + COLR_RESET)
    return error_count

if __name__ == "__main__":
    if len(sys.argv) > 1:
        error_count = 0
        for f in sys.argv[1:]:
            error_count += check_ml_glyphs_sans_shaping(f)
        if error_count == 0:
            print(COLR_OK + "All glyphs have either Unicode value or GSUB rules associated" + COLR_RESET)

        error_count = 0
        for f in sys.argv[1:]:
            error_count += check_ml_glyph_names_unicode(f)
        if error_count == 0:
            print(COLR_OK + "All glyphs with Malayam Unicode value follows standard naming convention" + COLR_RESET)
        sys.exit(error_count)
    else:
        print("Please pass some fonts for testing")
        sys.exit(1)
