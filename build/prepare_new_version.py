#!/usr/bin/env python
# Copyright 2024 Rajeesh KV <rajeeshknambiar@gmail.com>

import argparse
import fontforge as ff
import re
from datetime import date

def prepare_new_release(version="1.0", sfd_files=[]):
    for sfd in sfd_files:
        f = ff.open(sfd)
        if not f:
            print(f"Unable to open {sfd}")
            continue
        f.version = version
        # Assumes last year is followed by a space (e.g. 'Copyright 2021-2023 Author name...')
        cur_year = str(date.today().year)
        f.copyright = re.sub(r'\d{4} ', cur_year + ' ', f.copyright)
        # TTF names, in format (('Lang', 'StrID', 'Value'),...), containing Family, Version, etc.
        # Replace Version & Copyright in TTF names (appendSFNTName updates existing entry)
        for td in filter(lambda t: t[1] in {'Copyright','Version'}, f.sfnt_names):
            if td[1] == 'Version':
                f.appendSFNTName(td[0], td[1], version)
            if td[1] == "Copyright":
                f.appendSFNTName(td[0], td[1], re.sub(r'\d{4} ', cur_year + ' ', td[2]))

        f.save()

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-v", help="Version string")
    ap.add_argument("-s", nargs="+", help="Path to SFD file[s]")

    args = ap.parse_args()
    version = args.v if args.v else "1.0"
    sfd_files = args.s if args.s else []

    prepare_new_release(version, sfd_files)
